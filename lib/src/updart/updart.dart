part of updart;

Symbol _constructorSymbol(Symbol symbol) {
  String symbolName = MirrorSystem.getName(symbol);
  if(symbolName.contains("."))
    return new Symbol(symbolName.split("\.")[1]);
  else return const Symbol("");
}

Symbol _findConstructorWithArgs(ClassMirror classMirror, List<Object> pArgs, Map<Symbol, Object> nArgs) {
  Symbol constructor = null;

  for(Symbol symbol in classMirror.declarations.keys) {
    if(constructor != null)
      break;
    DeclarationMirror declarationMirror = classMirror.declarations[symbol];
    if(declarationMirror is MethodMirror && declarationMirror.isConstructor) {
      int numPos = 0;
      int numNamed = 0;
      List<Symbol> namedArgs = nArgs.keys.toList();
      for(ParameterMirror paramaterMirror in declarationMirror.parameters) {
        if(paramaterMirror.isNamed && !paramaterMirror.isOptional) {
          if(!namedArgs.contains(paramaterMirror.simpleName))

            //If non optional named arg is not present in constructor, non matching constructor
            break;
          numNamed++;
        } else if(paramaterMirror.isNamed && namedArgs.contains(paramaterMirror.simpleName)) {
          numNamed++;
        } else
          numPos++;
      }

      if(numPos == pArgs.length && numNamed == nArgs.length) {
        constructor = declarationMirror.simpleName;
      }
    }
  }

  return _constructorSymbol(constructor);
}