library updart;

import "dart:mirrors";
import "package:logging/logging.dart";

part "src/updart/updart.dart";

class Application {
  static Logger _logger = new Logger("Application");
  bool instantiated = false;
  Map<Symbol, RegistryEntry> _registry = {};
  static final Application _application = new Application._internal();

  Application._internal();

  factory Application() {
    return _application;
  }

  RegistryEntry register(Symbol symbol, Type type) {
    RegistryEntry registryEntry = new RegistryEntry(symbol, type);
    _registry.putIfAbsent(symbol, () => registryEntry);
    return registryEntry;
  }

  Object retrieve(Symbol symbol) {
    RegistryEntry registryEntry = _registry[symbol];
    if(registryEntry == null) {
      _logger.warning("Symbol $symbol not registered in application");
      return null;
    }
    if(registryEntry.instantiated == null)
      _instantiateRegistryEntry(registryEntry);
    Object object = registryEntry.instantiated;
    if(object is BeanFactory)
      return object.getObject();
    else
      return object;
  }

  void _instantiate() {
    _registry.forEach((Symbol symbol, RegistryEntry registryEntry) {
      if(registryEntry.instantiated == null)
        _instantiateRegistryEntry(registryEntry);
    });
  }

  void _instantiateRegistryEntry(RegistryEntry registryEntry) {
    ClassMirror classMirror = reflectClass(registryEntry.type);
    InstanceMirror im;

    if(!registryEntry._args.isEmpty || !registryEntry._namedArgs.isEmpty) {
      Symbol constructor = _findConstructorWithArgs(classMirror, registryEntry._args, registryEntry._namedArgs);
      im = classMirror.newInstance(constructor,
        _resolveList(registryEntry._args), _resolveMap(registryEntry._namedArgs));
    } else
      im = classMirror.newInstance(const Symbol(""), [], {});

    registryEntry.instantiated = im.reflectee;

    registryEntry._properties.forEach((Symbol symbol, Object value) {
      im.setField(symbol, _resolveValue(value));
    });
  }

  Object _resolveValue(Object value) {
    if(value is Symbol && _registry.containsKey(value))
      return retrieve(value);
    else
      return value;
  }

  List _resolveList(List values) {
    List l = [];
    for(var v in values) {
     l.add(_resolveValue(v));
    }
    return l;
  }

  Map _resolveMap(Map map) {
    Map m = {};
    map.forEach((k, v) {
      m[k] = _resolveValue(v);
    });
    return m;
  }
}

class RegistryEntry {
  final Symbol symbol;
  final Type type;
  Object instantiated;
  List<Object> _args = [];
  Map<Symbol, Object> _namedArgs = {};
  Map<Symbol, Object> _properties = {};

  RegistryEntry(this.symbol, this.type);

  posArgs(List<Object> args) => _args = args;
  property(Symbol symbol, Object object) => _properties[symbol] = object;
  namedArg(Symbol str, Object object) => _namedArgs[str] = object;
}

abstract class BeanFactory {
  getObject();
}