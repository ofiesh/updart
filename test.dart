import "package:updart/updart.dart";

class Foobar {
  final String string;
  final Bar bar2;
  int i;
  bool _b;
  Bar bar;
  Foobar.foo(this.string, this.bar2);
  set b(bool b) => _b = b;
  get b => _b;
}

class Bar {
  bool b;
}

int main() {
  Application application = new Application();
   application.register(#f, Foobar)
    ..posArgs(["abc", #b])
    ..property(#i, 3)
    ..property(#bar, #b)
    ..property(#b, true);
   application.register(#b, Bar)
    ..property(#b, false);

   Foobar f = application.retrieve(#f);
   print(f.i);
   print(f.string);
   print(f.b);
   print(f.bar2.b);
}